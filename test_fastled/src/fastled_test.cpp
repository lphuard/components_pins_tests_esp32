#define LED_WS2812B   0

// #define LTE_RTS   32
// #define LTE_TX    33
// #define LTE_RX    34
// #define LTE_CTS   35
// #define LORA_WAKE 38

// #define LORA_SS   18
// #define LORA_CLK  5
// #define LORA_MOSI 27
// #define LORA_MISO 19
// #define LORA_INT  23


#include "FastLED.h"
#define NUM_LEDS 1
CRGB leds[NUM_LEDS];

void setup() {
  // Set console baud rate (not used here)
  Serial.begin(115200);

  // Init RGB LED
  FastLED.addLeds<WS2812B, LED_WS2812B, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(32);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
}

void loop() {
  leds[0] = CRGB::Red;
  FastLED.show();
  delay(500);
  leds[0] = CRGB::Green;
  FastLED.show();
  delay(500);
  leds[0] = CRGB::Blue;
  FastLED.show();
  delay(500);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(500);
}
